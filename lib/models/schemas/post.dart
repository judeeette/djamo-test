
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@JsonSerializable()

class Post {
  int id;
  String title;
  String body;
  int userId;

  Post(
      {required this.id,
      required this.body,
      required this.title,
      required this.userId});

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);    
}
