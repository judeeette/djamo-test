import 'dart:convert';
import 'dart:io';

import 'package:ipost_flutter_app/app_config.dart';
import 'package:http/http.dart' as http;
import 'package:ipost_flutter_app/models/schemas/post.dart';

class MainApi {
  final AppConfig config;
  final http.Client client;
  final String endpoint = '/posts';

  MainApi({required this.client, required this.config});

  Future<List<Post>> getPost() async {
    final String url = config.apiBaseUrl + endpoint;
    final response = await client.get(Uri.parse(url), headers: {
      HttpHeaders.contentTypeHeader: 'application/json'
    });
    List<Post> responseMap = jsonDecode(response.body);
    if (response.statusCode != 200) {
      throw Exception("error");
    }
    return responseMap;
  }
}
