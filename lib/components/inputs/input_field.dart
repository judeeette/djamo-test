// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:ipost_flutter_app/constants/regex.dart';

Widget InputField(
    {required String initialValue,
    String? labelText,
    String? label,
    required TextInputType inputType,
    dynamic onChanged,
    bool obscureText = false,
    String? valueToCheck,
    bool showErrorMessage = true,
    Widget? prefixIcon,
    bool autofocus = false,
    int? maxLines,
    bool enabled = true}) {
  return Column(
    children: <Widget>[
      label != null
          ? Container(
              margin: const EdgeInsets.only(bottom: 5),
              alignment: Alignment.topLeft,
              child: Text(
                label,
                style: const TextStyle(
                    fontSize: 17.0, fontWeight: FontWeight.w600),
              ),
            )
          : Container(),
      TextFormField(
        enabled: enabled,
        maxLines: maxLines,
        autofocus: autofocus,
        obscureText: obscureText,
        initialValue: initialValue,
        onChanged: onChanged,
        validator: (value) {
          if (value!.isEmpty && showErrorMessage == true) {
            return 'Requis';
          } else if (inputType == TextInputType.emailAddress &&
              !emailRegEx.hasMatch(value) &&
              showErrorMessage == true) {
            return 'Adresse e-mail invalide';
          }
          return null;
        },
        decoration: InputDecoration(
          isDense: true,
          prefixIcon: prefixIcon ?? null,
          hintText: labelText,
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey)),
          floatingLabelBehavior: FloatingLabelBehavior.never,
          errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red)),
          labelStyle: const TextStyle(fontSize: 16),
          border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))),
        ),
      ),
    ],
  );
}
