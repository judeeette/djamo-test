// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:ipost_flutter_app/constants/colors.dart';

Widget Loader({
  double size = 20.0,
  Color color = colorOrange,
  double strokeWidth = 3,
}) {
  return SizedBox(
    height: size,
    width: size,
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(color),
      strokeWidth: strokeWidth,
    ),
  );
}
