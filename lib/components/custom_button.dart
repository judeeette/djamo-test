// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:ipost_flutter_app/components/loader.dart';
import 'package:ipost_flutter_app/constants/colors.dart';

Widget CustomButton({
  required BuildContext contextProp,
  required Function onPressedProp,
  required String textProp,
  bool disabledProp = false,
  bool loader = false,
  Color color = colorOrange,
  double? width,
}) {
  return IgnorePointer(
      ignoring: disabledProp,
      child: Opacity(
        opacity: disabledProp ? 0.5 : 1,
        child: TextButton(
          style: ButtonStyle(
              shape: MaterialStateProperty.all<OutlinedBorder>(
                  const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                Radius.circular(2),
              ))),
              backgroundColor: MaterialStateProperty.all(color)),
          onPressed: () {
            onPressedProp();
          },
          child: Container(
              alignment: Alignment.center,
              height: 30.0,
              width: width ?? MediaQuery.of(contextProp).size.width,
              child: loader
                  ? Loader(size: 15, color: Colors.white)
                  : Text(textProp,
                      style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontFamily: "Helvetica Neue",
                        fontSize: 17,
                        color: Colors.white,
                      ))),
        ),
      ));
}
