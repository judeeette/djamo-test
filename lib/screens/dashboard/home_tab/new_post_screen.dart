import 'package:flutter/material.dart';
import 'package:ipost_flutter_app/components/custom_button.dart';
import 'package:ipost_flutter_app/components/inputs/input_field.dart';
import 'package:ipost_flutter_app/components/tab_app_bar.dart';
import 'package:ipost_flutter_app/constants/colors.dart';

class NewPostScreen extends StatefulWidget {
  const NewPostScreen({Key? key}) : super(key: key);
  @override
  NewPostScreenState createState() => NewPostScreenState();
}

class NewPostScreenState extends State<NewPostScreen> {
  @override
  void initState() {
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  String? title = "";
  String? desc = "";
  bool loader = false;

  void onChangedTitle(String text) {
    setState(() {
      title = text;
    });
  }

  void onChangedDesc(String text) {
    setState(() {
      desc = text;
    });
  }

  void _add() {
    setState(() {
      loader = true;
    });

    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        loader = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TabAppBar(
          titleProp: "Ajouter un nouveau poste",
          context: context,
          centerTitle: true,
          showBackButton: true),
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(color: colorGreyShadeLight),
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: RefreshIndicator(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Form(
                                    key: _formKey,
                                    child: Column(
                                      children: <Widget>[
                                        InputField(
                                            initialValue: '',
                                            inputType: TextInputType.text,
                                            onChanged: onChangedTitle,
                                            labelText: "Titre"),
                                        const SizedBox(
                                          height: 10.0,
                                        ),
                                        InputField(
                                            initialValue: '',
                                            inputType: TextInputType.text,
                                            onChanged: onChangedDesc,
                                            labelText: "Description"),
                                        const SizedBox(
                                          height: 10.0,
                                        ),
                                        CustomButton(
                                            contextProp: context,
                                            loader: loader,
                                            color: colorGreenShade,
                                            onPressedProp: () {
                                              if (_formKey.currentState!
                                                  .validate()) {
                                                _add();
                                              }
                                            },
                                            textProp: "Ajouter un poste")
                                      ],
                                    ))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    onRefresh: () async {}))
          ],
        ),
      )),
    );
  }
}
