import 'package:flutter/material.dart';
import 'package:ipost_flutter_app/components/custom_button.dart';
import 'package:ipost_flutter_app/components/inputs/input_field.dart';
import 'package:ipost_flutter_app/components/loader.dart';
import 'package:ipost_flutter_app/components/tab_app_bar.dart';
import 'package:ipost_flutter_app/constants/colors.dart';

class ShowPostScreen extends StatefulWidget {
  const ShowPostScreen({Key? key}) : super(key: key);
  @override
  ShowPostScreenState createState() => ShowPostScreenState();
}

class ShowPostScreenState extends State<ShowPostScreen> {
  @override
  void initState() {
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  String? title = "";
  String? desc = "";
  bool loader = false;

  void onChangedTitle(String text) {
    setState(() {
      title = text;
    });
  }

  void onChangedDesc(String text) {
    setState(() {
      desc = text;
    });
  }

  void _update() {
    setState(() {
      loader = true;
    });

    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        loader = false;
      });
    });
  }

  void _delete() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TabAppBar(
          titleProp: "Détails et modification poste",
          context: context,
          centerTitle: true,
          showBackButton: true),
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(color: colorGreyShadeLight),
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: RefreshIndicator(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Form(
                                    key: _formKey,
                                    child: Column(
                                      children: <Widget>[
                                        InputField(
                                            initialValue: '',
                                            inputType: TextInputType.text,
                                            onChanged: onChangedTitle,
                                            labelText: "Titre"),
                                        const SizedBox(
                                          height: 10.0,
                                        ),
                                        InputField(
                                            initialValue: '',
                                            inputType: TextInputType.text,
                                            onChanged: onChangedDesc,
                                            labelText: "Description"),
                                        const SizedBox(
                                          height: 10.0,
                                        ),
                                        Row(
                                          children: <Widget>[
                                            TextButton(
                                                style: ButtonStyle(
                                                    shape: MaterialStateProperty
                                                        .all<OutlinedBorder>(
                                                            const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                      Radius.circular(2),
                                                    ))),
                                                    backgroundColor:
                                                        MaterialStateProperty
                                                            .all(colorOrange)),
                                                child: loader
                                                    ? Loader()
                                                    : const Text(
                                                        'Modifier',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                onPressed: () {
                                                  if (_formKey.currentState!
                                                      .validate()) {
                                                    _update();
                                                  }
                                                }),

                                            // CustomButton(
                                            //     contextProp: context,
                                            //     loader: loader,
                                            //     color: colorOrange,
                                            //     onPressedProp: () {
                                            //       if (_formKey.currentState!
                                            //           .validate()) {
                                            //         _update();
                                            //       }
                                            //     },
                                            //     textProp: "Modifier"),
                                            const SizedBox(
                                              width: 10.0,
                                            ),
                                            TextButton(
                                                style: ButtonStyle(
                                                    shape: MaterialStateProperty
                                                        .all<OutlinedBorder>(
                                                            const RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .all(
                                                      Radius.circular(2),
                                                    ))),
                                                    backgroundColor:
                                                        MaterialStateProperty
                                                            .all(Colors.red)),
                                                child: loader
                                                    ? Loader()
                                                    : const Text(
                                                        'Supprimer',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                onPressed: () {
                                                  if (_formKey.currentState!
                                                      .validate()) {
                                                    _delete();
                                                  }
                                                }),
                                          ],
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    onRefresh: () async {}))
          ],
        ),
      )),
    );
  }
}
