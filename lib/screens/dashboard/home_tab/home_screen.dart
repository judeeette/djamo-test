import 'package:flutter/material.dart';
import 'package:ioc/ioc.dart';
import 'package:ipost_flutter_app/components/custom_button.dart';
import 'package:ipost_flutter_app/components/custom_dialog.dart';
import 'package:ipost_flutter_app/components/tab_app_bar.dart';
import 'package:ipost_flutter_app/constants/colors.dart';
import 'package:ipost_flutter_app/models/schemas/post.dart';
import 'package:ipost_flutter_app/screens/dashboard/home_tab/new_post_screen.dart';
import 'package:ipost_flutter_app/screens/dashboard/home_tab/show_post_screen.dart';
import 'package:ipost_flutter_app/services/main_api.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    //_getPost();
    super.initState();
  }

  MainApi mainApi = Ioc().use('mainApi');

  void _getPost() async {
    try {
      final response = await mainApi.getPost();
    } catch (e) {
      _showDialog(e.toString());
    }
  }

  _showDialog(String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomDialog(contextProp: context, messageProp: message);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TabAppBar(titleProp: "Home", context: context, centerTitle: true),
      body: SafeArea(
          child: Container(
        decoration: BoxDecoration(color: colorGreyShadeLight),
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: RefreshIndicator(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                CustomButton(
                                    contextProp: context,
                                    color: colorGreenShade,
                                    onPressedProp: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                const NewPostScreen(),
                                          ));
                                    },
                                    textProp: "Ajouter un nouveau poste"),
                                const SizedBox(
                                  height: 20.0,
                                ),
                                const Text('Liste des postes'),
                                const SizedBox(
                                  height: 10.0,
                                ),
                                Container(
                                    child: Column(
                                  children: <Widget>[
                                    Container(
                                      margin:
                                          const EdgeInsets.only(bottom: 10.0),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Colors.white),
                                      child: ListTile(
                                        leading: const Icon(
                                            Icons.remove_red_eye_outlined),
                                        trailing: const Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Colors.black,
                                          size: 30.0,
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    const ShowPostScreen(),
                                              ));
                                        },
                                        title: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "titre 1",
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            const SizedBox(
                                              height: 8.0,
                                            ),
                                            Text("body 1")
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin:
                                          const EdgeInsets.only(bottom: 10.0),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Colors.white),
                                      child: ListTile(
                                        leading: const Icon(
                                            Icons.remove_red_eye_outlined),
                                        trailing: const Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Colors.black,
                                          size: 30.0,
                                        ),
                                        onTap: () {},
                                        title: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "titre 2",
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            const SizedBox(
                                              height: 8.0,
                                            ),
                                            Text("body 2")
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    onRefresh: () async {}))
          ],
        ),
      )),
    );
  }
}
