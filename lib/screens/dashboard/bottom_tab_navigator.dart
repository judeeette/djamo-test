import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ipost_flutter_app/screens/dashboard/home_tab/home_router.dart';
import 'package:ipost_flutter_app/screens/dashboard/router_page.dart';

class BottomTabNavigator extends StatefulWidget {
  const BottomTabNavigator({Key? key}) : super(key: key);

  @override
  BottomTabNavigatorState createState() => BottomTabNavigatorState();
}

class BottomTabNavigatorState extends State<BottomTabNavigator> {
  int _selectedIndex = 0;
  bool _bottomTabNavigatorVisible = true;
  static const activeColor = Colors.black;
  static const normalColor = Colors.grey;

  hideBottomTabNavigator() {
    setState(() {
      _bottomTabNavigatorVisible = false;
    });
  }

  showBottomTabNavigator() {
    setState(() {
      _bottomTabNavigatorVisible = true;
    });
  }

  final List _widgetOptions = <dynamic>[
    HomeRouter(),
    HomeRouter(),
  ];

  void onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> _onBackPressed() async {
    if (_widgetOptions[_selectedIndex] is RouterPage) {
      final RouterPage page = _widgetOptions[_selectedIndex] as RouterPage;
      return !await page.navigatorKey.currentState!.maybePop();
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            body: Center(
              child: _widgetOptions.elementAt(_selectedIndex),
            ),
            bottomNavigationBar: _bottomTabNavigatorVisible
                ? BottomNavigationBar(
                    type: BottomNavigationBarType.fixed,
                    backgroundColor: Colors.white,
                    showUnselectedLabels: true,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/home.svg',
                          color: activeColor,
                          width: 20,
                          height: 20,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/home.svg',
                          color: normalColor,
                          width: 20,
                          height: 20,
                        ),
                        label: 'Accueil',
                      ),
                      BottomNavigationBarItem(
                        activeIcon: SvgPicture.asset(
                          'lib/assets/svgs/home.svg',
                          color: activeColor,
                          width: 20,
                          height: 20,
                        ),
                        icon: SvgPicture.asset(
                          'lib/assets/svgs/home.svg',
                          color: normalColor,
                          width: 20,
                          height: 20,
                        ),
                        label: 'test',
                      ),
                    ],
                    currentIndex: _selectedIndex,
                    selectedItemColor: activeColor,
                    unselectedItemColor: normalColor,
                    onTap: onItemTapped,
                  )
                : null));
  }
}
