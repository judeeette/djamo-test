// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ioc/ioc.dart';
import 'package:ipost_flutter_app/app_config.dart';
import 'package:ipost_flutter_app/locator.dart';
import 'package:ipost_flutter_app/services/navigation_service.dart';
import 'package:ipost_flutter_app/screens/dashboard/bottom_tab_navigator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class RootScreen extends StatefulWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  RootScreenState createState() => RootScreenState();
}

class RootScreenState extends State<RootScreen> with WidgetsBindingObserver {
  String _token = '';
  DateTime? _backgroundTime;

  final NavigationService _navigationService = locator<NavigationService>();
  final storage = Ioc().use<FlutterSecureStorage>('secureStorage');

  @override
  void initState() {
    super.initState();

    _subscribeAppLifeCycle();
    _readToken();
  }

  Future<void> _readToken() async {
    final tokenPref = await storage.read(key: 'token');
    setState(() {
      _token = tokenPref;
    });
  }

  void _subscribeAppLifeCycle() {
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    _unsubscribeAppLifeCycle();
  }

  void _unsubscribeAppLifeCycle() {
    WidgetsBinding.instance!.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _lockScreenIfNecessary();
    } else if (state == AppLifecycleState.paused) {
      _startLockTimer();
    }
  }

  void _lockScreenIfNecessary() async {
    if (_backgroundTime != null) {
      if (await _isLockNecessary()) {
        // unawaited(
        //     _navigationService.popAllAndNavigateTo(MaterialPageRoute<void>(
        //   builder: (BuildContext context) => PasscodeScreen(),
        // )));
      }
    }
    _backgroundTime = null;
  }

  Future<bool> _isLockNecessary() async {
    final currentTime = DateTime.now();
    final elapsedDuration = currentTime.difference(_backgroundTime!);
    final lockInSeconds = AppConfig.of(context)!.lockInSeconds;

    await _readToken();
    return elapsedDuration.inSeconds > lockInSeconds && _token != null;
  }

  void _startLockTimer() {
    _backgroundTime ??= DateTime.now();
  }

  Widget _getHomeScreen() =>
      const BottomTabNavigator();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      supportedLocales: const [Locale('fr', 'FR')],
      navigatorKey: _navigationService.navigationKey,
      home: _getHomeScreen(),
    );
  }
}
