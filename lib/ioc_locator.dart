// ignore_for_file: import_of_legacy_library_into_null_safe

// ignore: unused_import
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ioc/ioc.dart';
import 'package:ipost_flutter_app/app_config.dart';
import 'package:ipost_flutter_app/http_client_config.dart';
import 'package:ipost_flutter_app/services/main_api.dart';
import 'package:ipost_flutter_app/services/safe_secure_storage.dart';

void iocLocator(AppConfig config) {
  Ioc().bind(
      'mainApi', (ioc) => MainApi(config: config, client: initHttpClient()));
  Ioc().bind('secureStorage',
      (ioc) => SafeSecureStorage(const FlutterSecureStorage()));
}
