import 'package:get_it/get_it.dart';
import 'package:ipost_flutter_app/app_config.dart';
import 'package:ipost_flutter_app/services/navigation_service.dart';

final locator = GetIt.instance;

void setupLocator(AppConfig config) {
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
}
