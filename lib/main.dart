// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:ipost_flutter_app/app_config.dart';
import 'package:ipost_flutter_app/ioc_locator.dart';
import 'package:ipost_flutter_app/locator.dart';
import 'package:ipost_flutter_app/screens/root/root_screen.dart';
import 'package:package_info/package_info.dart';

const String DEFAULT_LOCALE = 'FR';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  var configuredApp = AppConfig(
    appName: 'iPost',
    apiBaseUrl: 'https://jsonplaceholder.typicode.com',
    flavorName: 'production',
    lockInSeconds: 60,
    version: packageInfo.version,
    child: const AppMain(),
  );
  iocLocator(configuredApp);
  setupLocator(configuredApp);
  runApp(configuredApp);
}

class AppMain extends StatelessWidget {
  const AppMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const RootScreen();
  }
}
