import 'package:http_interceptor/http_interceptor.dart';
import 'package:ipost_flutter_app/interceptors/api_language_interceptor.dart';

InterceptedClient initHttpClient() =>
    InterceptedClient.build(interceptors: [ApiLanguageInterceptor()]);
