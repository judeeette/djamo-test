import 'package:flutter/material.dart';

const Color colorBlue = Color(0xFF41ACEF);
const Color colorOrange = Color(0xFFF57C00);
const Color colorPurple = Color(0xFFBA68C8);
const Color colorGreenShade = Color(0xFF66BB6A);
const Color colorGrey = Color(0xFF9E9E9E);
const Color colorOrangeShade = Color(0xFFFFE0B2);

const Color colorGreyShadeLight = Color(0xFFFAFAFA);
